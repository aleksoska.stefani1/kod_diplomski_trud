import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from gpiozero import MotionSensor
from picamera import PiCamera
import time
import datetime

pir = MotionSensor(12)
camera = PiCamera()

def send_email(image_path):
    smtp_server = 'smtp.gmail.com'
    smtp_port = 587
    smtp_username = 'aleksoska.stefani1@gmail.com'
    smtp_password = '****************'
 
    sender_email = 'aleksoska.stefani1@gmail.com'
    receiver_email = 'aleksoska.stefani1@gmail.com'
    subject = 'Motion Detected!'
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    body = f'Motion has been detected at {current_time}. See the attached image.'

    message = MIMEMultipart()
    message['From'] = sender_email
    message['To'] = receiver_email
    message['Subject'] = subject

    message.attach(MIMEText(body, 'plain'))

    with open(image_path, 'rb') as attachment:
        image_mime = MIMEImage(attachment.read(), _subtype="jpeg")
        attachment.close()
        message.attach(image_mime)

    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(smtp_username, smtp_password)
        server.send_message(message)

while True:
    pir.wait_for_motion()
    print("Motion detected!")
    filename = "/home/stefani/Desktop/" + (time.strftime("%y%b%d_%H:%M:%S")) + ".jpg"
    time.sleep(1)
    camera.start_preview(fullscreen=False, window=(1250, 10, 640, 480))
    camera.capture(filename)
    camera.stop_preview()
    send_email(filename)
    pir.wait_for_no_motion()
